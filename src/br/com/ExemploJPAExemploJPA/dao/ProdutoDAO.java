/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ExemploJPAExemploJPA.dao;

import br.com.ExemploJPAExemploJPA.modelo.Categoria;
import br.com.ExemploJPAExemploJPA.modelo.Produto;
import java.util.List;
import javax.persistence.TypedQuery;

/**
 *
 * @author sala303b
 */
public class ProdutoDAO extends DAO<Produto> {

    public ProdutoDAO() {
        super(Produto.class);
    }

    public List<Produto> findBy(String descricao, Categoria categoria) {

        this.em = JPAUtil.getEntityManager();
        String hql = "select p from Produto p join p.categoria c where 1=1 ";

        if (!descricao.equals("")) {
            hql += " and p.descricao like :descricao";
        }

        if (categoria != null) {
            hql += " and c = :categoria";
        }

        TypedQuery<Produto> query = em.createQuery(hql, Produto.class);

        if (!descricao.equals("")) {
            query.setParameter("descricao", "%" +  descricao  + "%");
        }

        if (categoria != null) {
            query.setParameter("categoria", categoria);
        }

        List<Produto> lista = query.getResultList();

        return lista;
    }

}
